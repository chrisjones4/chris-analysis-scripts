import signac
import freud
import msd_plot as msd
import rdf_plot as rdf
import os
import sys

def group_by_size():
    
    num_mols = len(varied_parm_dict['molecule'])
    print("NUM OF MOLS = {}".format(num_mols))

def build_dict(parameters, scheme):

    parm_dict = {}
    for p in parameters:
        values = sorted(list(list(scheme[p].values())[0]))
        parm_dict.update({p:values})
    return parm_dict

def plot_single(variable_parameters, variable_range, function):
    
    if function == 'msd':
        file_ext = 'msd.log'
    elif function == 'rdf':
        file_ext = 'centers.gsd'
    var_values = sorted(varied_parm_dict[variable_parameters[0]])
    if variable_range:
        var_values = [i for i in var_values if i>=parm_range[0] and i<=parm_range[-1]]
    job_ids = []
    for N, group in project.groupby(variable_parameters[0]):
        if N in var_values:
            ids = [x.get_id() for x in group]
            job_ids.append(ids[0])
    job_files = [project.get_job("workspace/{}".format(i)).fn(file_ext)
            for i in job_ids]
    variable_labels = [project.get_statepoint(i)[variable_parameters[0]] for i in job_ids]
    if variable_parameters[0] == 'kT_reduced':
        density = project.get_statepoint(job_ids[0])['density']
        if function == 'msd':
            msd.plot_msd(files = job_files,
                    labels = False,
                    temperatures = variable_labels,
                    densities = [density],
                    skip_frames = 0,
                    group_type = 'centers',
                    title = 'MSD')
        elif function == 'rdf':
            rdf.plot_rdf(files = job_files,
                    labels = False,
                    temperatures = variable_labels,
                    densities = [density],
                    num_frames = 5,
                    title = 'RDF')

    elif variable_parameters[0] == 'density':
        temperature = project.get_statepoint(job_ids[0])['kT_reduced']
        if function == 'msd':
            msd.plot_msd(files = job_files,
                    labels = False,
                    temperatures = [temperature],
                    densities = variable_labels,
                    skip_frames = 0,
                    group_type = 'centers',
                    title = 'MSD at Constant Temperature of {} kT'.format(temperature))
        elif function == 'rdf':
            rdf.plot_rdf(files = job_files,
                    labels = False,
                    temperatures = variable_labels,
                    density = [density],
                    num_frames = 5,
                    title = 'RDF at Constant Density of {}'.format(density))
    elif variable_parameters[0] == 'n_compounds':
        pass

def plot_set(variable_parameters, omitted_parameter, function): # Pass in a list of variable parameters ['kT_reduced', 'density']

    if function == 'msd':
        file_ext = 'msd.log'
    elif function == 'rdf':
        file_ext = 'centers.gsd'
    for omitted_var in omitted_parameter:
        omitted_var_values = sorted(varied_parm_dict[omitted_var])
        for omitted_val in omitted_var_values:
            
            print("OMITTED VARIABLE = {}".format(omitted_var))
            print("VALUES = {}".format(omitted_var_values))
            if not os.path.isdir(os.path.join(os.getcwd(), 'output-figs/{}/{}'.format(omitted_var, omitted_val))):
                os.mkdir('output-figs/{}/{}'.format(omitted_var, omitted_val))
            for i, var in enumerate(variable_parameters):
                print(var)
                constants = sorted(varied_parm_dict[var]) # Of the variable parms, one of them is picked first to be held constant at each of its values
                try:
                    variable = variable_parameters[i + 1] # At each constant, single msd plot is created at each value in variable
                    #variable_values = sorted(varied_parm_dict[variable])
                except:
                    variable = variable_parameters[i - 1]
                    #variable_values = variable_dict[variable]
                for constant in constants:
                    print('{} = {}'.format(var, constant))
                    print('{} = {}'.format(omitted_var, omitted_val))
                    constant_ids = project.find_job_ids({omitted_var: omitted_val, var: constant})
                    print(constant_ids)
                    constant_files = [project.get_job("workspace/{}".format(job_id)).fn(file_ext) for job_id in constant_ids]
                    print(constant_files)
                    variable_labels = [project.get_statepoint(i)[variable] for job_id in constant_ids]
                    print(variable_labels)
                    exit()
            for N, group in project.groupby(var):
                if N == constant:
                    constant_ids = [x.get_id() for x in group]
                    # TO DO:  Sort the ids in order of the variable values
                    # Sorted dict using the costant_ids and their temp values?
                    constant_files = [project.get_job("workspace/{}".format(i)).fn(file_ext)
                                                       for i in constant_ids]
                    variable_labels = [project.get_statepoint(i)[variable]
                                       for i in constant_ids]
                    print('{} = {}'.format(var, constant))
                    print(variable_labels)
                    print()
                    if function == 'msd':
                        title = 'MSD at {}={}'.format(var, constant)
                        if var == 'density':
                            msd.plot_msd(files = constant_files,
                                        labels = False, 
                                        temperatures = variable_labels,
                                        densities = [constant],
                                        skip_frames = 0,
                                        group_type = 'centers',
                                        title = title)
                        elif var == 'kT_reduced':
                            msd.plot_msd(files = constant_files,
                                        labels = False,
                                        temperatures = [constant],
                                        densities = variable_labels,
                                        skip_frames = 0,
                                        group_type = 'centers',
                                        title = title)
                    elif function == 'rdf':
                        title = 'RDF at {}={}'.format(var, constant)
                        if var == 'density':
                            rdf.plot_rdf(files = constant_files,
                                        labels = False,
                                        temperatures = variable_labels,
                                        densities = [constant],
                                        num_frames = 5,
                                        title = title)
                        elif var == 'kT_reduced':
                            rdf.plot_rdf(files = constant_files,
                                        labels = False,
                                        temperatures = [constant],
                                        densities = variable_labels,
                                        num_frames = 5,
                                        title = title)
    
def main(parms = ['density', 'kT_reduced', 'molecule', 'n_compounds'], parm_range = None, function = 'msd'):

    '''
    Main function that initializes reading in the project and finding parameters.
    Builds dictionaries of simulation parameters and their values.
    Finds the variable simulation parameters of the project.
    
    parms: list
        NOT WORKING YET
        List of variable parameters that you are interested in.

    parm_range: Not functional yet, still working.

    function: str
        Specify the type of function you want to plot.
        Options include 'msd' and 'rdf'

    variables:
        parameter dictionaries (all parameters, variable parameters only)
        single_variable - True if the project only contains a single simulation variable
        variable_list - A list of simulation parameters that have been varied within the project
        parameter_combos - A list of lists. Contains each possible combination of 2 variable parameters
    '''

    global project
    global varied_parm_dict
    global all_parm_dict

    if not os.path.isdir(os.path.join(os.getcwd(),'output-figs')):
        os.mkdir('output-figs')
    if not os.path.isdir(os.path.join(os.getcwd(), 'output-figs/{}'.format(function))):
        os.mkdir('output-figs/{}'.format(function))
    project = signac.get_project()
    pscheme = project.detect_schema(exclude_const=False) # Returns only the parms that have multiple values
    all_parameters = [list(i)[0] for i in pscheme.keys()]
    varied_parameters = [i for i in all_parameters if len(list(list(pscheme[i].values())[0])) > 1]
    print(varied_parameters)
    varied_parm_dict = build_dict(varied_parameters, pscheme)
    all_parm_dict = build_dict(all_parameters, pscheme)
    if 'molecule' in varied_parameters:
        group_by_size()
        exit()
    if len(varied_parameters) <= 1:
        single_variable = True
    else:
        single_variable = False
    if parms == None:
        variable_list = varied_parameters
    else:
        variable_list = list(set(parms) & set(varied_parameters))
        print('VARIABLE_LIST = {}'.format(variable_list))
        omitted = list(set(parms) - set(varied_parameters))
    #    print('OMITTED = {}'.format(omitted))
        omitted_variables = [i for i in list(set(parms) - set(varied_parameters)) if i in varied_parameters]

    # Make all combinations of 2 of the contents of variable_list
    if not single_variable:
        for i in variable_list:
            if not os.path.isdir(os.path.join(os.getcwd(),'output-figs/{}'.format(i))):
                os.mkdir('output-figs/{}'.format(i))
        parameter_combos = [[i,j] for i in variable_list[:-1] for j in variable_list[1:] if j != i]
        print('PARM COMBOS = {}'.format(parameter_combos))
        for combo in parameter_combos:
            omitted_parm = [i for i in variable_list if i not in combo]
            print("COMBO PARMS = {}".format(combo))
            print("OMITTED_PARM = {}".format(omitted_parm))
            plot_set(combo, omitted_parm, function)
    elif single_variable:
        plot_single(variable_list, parm_range, function = function)
        # Plot all of the jobs in the project on a single msd plot

if __name__ == "__main__":
    function = sys.argv[1]
    main(function = function)
