import freud
import gsd.fl
import gsd.hoomd
import numpy as np
import matplotlib.pyplot as plt

def find_starting_frame(trajectory):

    box_dims = trajectory[-1].configuration.box[:3]
    for i in range(0, len(trajectory)):
        if np.array_equal(trajectory[i].configuration.box[:3], box_dims):
            starting_index = i
            break
    return(starting_index)


def plot_rdf(files, labels, temperatures = False, densities = False, title = 'RDF',
                num_frames=1, r_max=8, dr=0.25, path = 'output-figs/rdf'):

    if temperatures or densities:
        if len(temperatures) > 1 and len(densities) > 1:
            print('Multiple values were provided for both temperatures and densities')
            print('As of now, this function only works with 1 constant and 1 variable.')
            return
        elif len(temperatures) > 1:
            labels = ['{} kT'.format(temp) for temp in temperatures]
            variable = 'Temperature'
            variable_values = temperatures
            constant = 'Density'
            constant_value = densities[0]
        elif len(densities) > 1:
            labels = ['{} den'.format(density) for density in densities]
            variable = 'Density'
            variable_values = densities
            constant = 'Temperature'
            constant_value = temperatures[0]
    print('LABELS = {}'.format(labels))
    fig = plt.figure()
    for i, f in enumerate(files):
        label = labels[i]
        print(label)
        rdf, x, y = average_rdf(f, num_frames, r_max, dr)
        plt.plot(x, y, label = label)
    plt.legend()
    plt.title(title)
    plt.ylabel('g(r)')
    plt.xlabel('r(σ)')
    plt.savefig('{}/{}.pdf'.format(path, title))


def average_rdf(file, num_frames=1, r_max=8, dr=0.25):

    gsd_file = gsd.fl.GSDFile(file, "rb")
    trajectory = gsd.hoomd.HOOMDTrajectory(gsd_file)
    starting_frame = find_starting_frame(trajectory) # First frame at final box dims
    print("STARTING FRAME = {}".format(starting_frame))
    if num_frames > len(trajectory) - starting_frame:
        print('Using this many frames will result in multiple box sizes being used')
        return
    x, y, z = trajectory[-1].configuration.box[:3]
    if r_max == None:
        if x == y and x == z:
            r_max = x // 2
        else:
            r_max = 8
    system_box = freud.box.Box(x, y, z)
    rdf = freud.density.RDF(r_max, dr)
    for i in range(1, num_frames + 1):
        frame = trajectory[-i]
        rdf = single_frame_rdf(frame=frame, rdf=rdf, box=system_box)
    x_data = rdf.R
    y_data = rdf.RDF
    return rdf, x_data, y_data

def single_frame_rdf(frame, rdf, box):

    positions = frame.particles.position
    rdf.accumulate(box=box, ref_points = positions)
    return rdf
