#!/Users/evanmiller/anaconda/bin/python
"""
The purpose of this script is to calculate the number of clusters that exist in a simulation.
"""

#Import the module files
import sys
import numpy as np
import mdtraj as md
from mpl_toolkits.mplot3d import Axes3D
import helper_functions.e_helper as cg

"""Check for hoomdxml file and create if necessary."""
cg.check_hoomdxml()

#Get values from trajectory and topologies
TRAJ_FILE = "traj.dcd" #Trajectory file
TOP_FILE = "restart.hoomdxml" #Topology file
t = md.load(TRAJ_FILE, top = TOP_FILE)

"Can be used if you want to visualize the average points."
#fig=plt.figure()
#ax= fig.add_subplot(111, projection='3d')

#These lines are for the different variables to use in loops.
frames = (t.n_frames-1)#gets the final frame

#print("Cheat sheet:\nPerylene (UA): APM = 20, indices = 4, 8, 11")
#print("Perylothiophene: APM = 21, indices = 4, 8, 11")
#print("Methyl-Perylene-diimide: APM = 32, indices = 4, 8, 11")
#print("DPB: APM = 64, indices = 17, 18, 33")
atoms_per_molecule = int(sys.argv[1])
atom1 = int(sys.argv[2])
atom2 = int(sys.argv[3])
atom3 = int(sys.argv[4])

#Calculate the number of molecules based off of the total number
#of atoms and atoms per molecule.
n_mol = int(t.n_atoms/atoms_per_molecule)

#Get the length of the unit cell edges.
axes = t.unitcell_lengths[t.n_frames-1]

dist = 0.334# The distance to determine if two molecules are in the same cluster. 
#NOTE:Units are nm from md.traj.

#Dot product cut off to be considered in the same cluster.
dot_cut = 0.9745

#Setting up the empty lists.
#Lists to place points used for the averages in cm_list.
p1_list = []
p2_list = []
p3_list = []
cm_list = []#An empty list for the average positions/center of mass
n_list = [ [] for i in range(n_mol) ]#List for the neighbors.
cp_list = [] #for the cross product.

def pbc(vec):
    """pbc is to account for the periodic boundry conditions. """
    for i,v in enumerate(vec):
        if v>axes[i]/2.:
            vec[i]=v-axes[i]
        if v<-axes[i]/2.:
            vec[i]=v+axes[i]
    return vec

def average_position(p1, p2, p3):
    """ This function takes the points, calculates vectors so the periodic boundries can be tested then will average and append the cm list."""
    v1 = pbc(p3 - p1)
    v2 = pbc(p3 - p2)
    p1 = p3 - v1
    p2 = p3 - v2
    average = (p1+p2+p3) / 3.
    return average

def cross_product(p1, p2, p3):
    """Calculates the cross product from three members of a ring"""
    v1 = pbc(p3 - p1)
    v2 = pbc(p3 - p2)
    cp = np.cross(v1, v2)
    cp /= np.linalg.norm(cp)
    return cp

def update_neighbors(particle, cluster_list, neighbor_list):
    """Recursive function to convert neighborlist into cluster list"""
#    print "calling function", particle
    for n in neighbor_list[particle]:
#        print "checking neighbor", particle,":", n
        if cluster_list[n]>cluster_list[particle]:
#            print "updating", n,"to" ,cluster_list[particle]
            cluster_list[n] = cluster_list[particle]
            cluster_list = update_neighbors(n,cluster_list,neighbor_list)
        elif cluster_list[n] < cluster_list[particle]: 
#            print "Updating", cluster_list[particle], "to", cluster_list[n]
            cluster_list[particle] = cluster_list[n]
            cluster_list = update_neighbors(particle,cluster_list,neighbor_list)
    return cluster_list

"""The indices over which you want to loop."""
p1_list = t.xyz[frames,atom1:-1:atoms_per_molecule,:]
p2_list = t.xyz[frames,atom2:-1:atoms_per_molecule,:]
p3_list = t.xyz[frames,atom3:-1:atoms_per_molecule,:]
for i in range(n_mol):
    cm_list.append(average_position(p1_list[i], p2_list[i], p3_list[i]))
    cp_list.append(cross_product(p1_list[i], p2_list[i], p3_list[i]))

#convert the list of arrays into an array of arrays.
cm_list = np.array(cm_list)
cp_list = np.array(cp_list)

#Build the neighborlist
for i in range(0,len(cm_list)-1):
    for j in range(i+1,len(cm_list)):
        a = cm_list[i,:]
        b = cm_list[j,:]
        d = np.linalg.norm(pbc(b-a))
        if  d < dist :
            cp1 = cp_list[i,:]
            cp2 = cp_list[j,:]
            dot = abs(np.dot(cp1, cp2))
            if dot > dot_cut: 
                n_list[i].append(j)
                n_list[j].append(i)
               
"""Print neighbors for each molecule."""
#for i,neighbors in enumerate(n_list):
#    print(i, neighbors)

"""Create a cluster list from neighbor list."""
c_list=[i for i in range(len(n_list))]
for i in range(len(c_list)):
    c_list = update_neighbors(i,c_list,n_list)

"""Show which residues are in which clusters."""
#for i in range(len(n_list)):
#    inclust = ""
#    for j,c in enumerate(c_list):
#        if c==i:
#            inclust+=str(j)+" "
#    if inclust !="":
#        print i,inclust
#print len(set(c_list)), "clusters"

"""Determine the overall order of the system"""
print(c_list)
order_list = []
for i in range(0, np.amax(c_list)+1):
    """Print cluster sizes and cluster members."""
#    if c_list.count(i) != 0:
#        print "Cluster", i, "has", c_list.count(i), "members"
    order = float(c_list.count(i))/float(len(c_list)) 
    if order >= 0.2:#Chosen from a dummy system.
        order_list.append(order)
#        print order
print(np.sum(order_list))

"""Create a script that colors clusters for easier viewing in VMD."""
if atoms_per_molecule == 64:
    print("I'm assuming this is DPB!")
    tcl_script = cg.DPBTCLscript(n_list, c_list)
else:
    tcl_script = cg.TCLscript(n_list, c_list)
